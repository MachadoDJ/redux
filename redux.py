#-*- coding: utf-8 -*-

# redu.py

# version 20150813

"""
LICENSE & AUTHORSHIP

Copyright (C) 2014 - Denis Jacob Machado
GNU General Public License version 3.0
Email: denisjacobmachado@gmail.com
Homepage: https://about.me/machadodj

REQUIREMENTS

Requires Biopython libraries

DESCRIPTION

Removes exact duplications from single end reads in
fast(a/q)(.gz) format

CRITERIA

Given two reads, if one read is perfectly included in the
other, keep the (1) longest read or (2) the read with better
average quality if both have the same length
"""

############################################################

#############
# ARGS&LIBS #
#############

try:
     import sys
except:
    sys.stderr.write(">ERROR: Could not import sys\n")
    sys.stderr.flush()
    exit()
try:from multiprocessing import Process,Queue,cpu_count
except:
    sys.stderr.write(">ERROR: Could not import Process and Queue from multiprocessing\n")
    sys.stderr.flush()
    exit()
try:import argparse,os
except:
    sys.stderr.write(">ERROR: importing general libraries\n")
    sys.stderr.flush()
    exit()
try:from Bio import SeqIO
except:
    sys.stderr.write(">ERROR: Could not import SeqIO from Bio\n")
    sys.stderr.flush()
    exit()
try:from Bio.Seq import Seq
except:
    sys.stderr.write(">ERROR: Could not import Seq from Bio.Seq\n")
    sys.stderr.flush()
    exit()
try:from Bio.Alphabet import generic_dna
except:
    sys.stderr.write(">ERROR: Could not import generic_dna from Bio.Alphabet\n")
    sys.stderr.flush()
    exit()

parser=argparse.ArgumentParser()
parser.add_argument("-i","--input",help="a single input file",metavar="<STRING>",type=str)
parser.add_argument("-f","--format",help="format of query sequences ['fasta','fastq'] (default = fastq)",metavar="<STRING>",type=str,default="fastq")
parser.add_argument("-k","--keep",help="number of copies to be kept (default = 5)",metavar="<INTEGER>",type=int,default=5)
parser.add_argument("-p","--processes",help="maximum processes to run simultaneously (default = max. number of cpus)",metavar="<INTEGER>",type=int,default=cpu_count())
parser.add_argument("-z","--gzip",help="use when input files are gunziped",action="store_true",default=False)
parser.add_argument("-v","--verbose",help="increase output verbosity",action="store_true",default=False)
parser.add_argument("-V","--version",help="print version number and exit",action="store_true",default=False)

args=parser.parse_args()

if(args.version):
    sys.stdout.write("version 2015.08.13\n")
    sys.stdout.flush()
    exit()

if(args.gzip):
    try:import gzip
    except:
        sys.stderr.write(">ERROR: could not import gzip\n")
        sys.stderr.flush()
        exit()

############################################################

##################
# MAIN FUNCTIONS #
##################

def main(): # parse input files
    """
    Read sequences one by one until there are N sequences in memory, where N is the number
    of processors. Then, each processor will compare one of theses reads against the
    remaining reads in the file. Unique reads will be printed on the screen.
    """
    uniq={}
    """
    'uniq' is a temporary dictionary of unique reads that will hold as many unique
    reads as the number of processes selected.
    """
    skip=[]
    """
    Save the number of the reads that have already been analyzed.
    """
    readno=0
    handle=openfile()
    if(args.verbose):
        sys.stderr.write(">Reding from %s\n"%(args.input))
        sys.stderr.flush()
    for seqrec in SeqIO.parse(handle,args.format):
        readno+=1
        if(args.verbose):
            sys.stderr.write("-- read %d\n"%(readno))
            sys.stderr.flush()
        if readno in skip:
             pass
        else:
            skip+=[readno]
            check=False
            for key in uniq:
                check=fit(str(seqrec.seq),key)
                if check==True:
                    uniq=addread(seqrec,key,uniq)
                    break
            if(check==False):
                uniq=addread(seqrec,'',uniq)
            if(len(uniq)==args.processes):
                """
                When the number of unique reads is the same as the number of processes, each
                process will take a single unique read and compare it to the remaining reads in
                file.
                """
                skip=prepareList(skip)
                skip=createJobs(uniq,skip)
                uniq={} # clean up memory
    handle.close()
    if(len(uniq)>0):
        for key in uniq:
            for rec in uniq[key][0]:
                sys.stdout.write("%s"%(rec.format(args.format.lower())))
                sys.stdout.flush()
        uniq={}
    if(args.verbose):
        sys.stderr.write(">FINISHED\n")
        sys.stderr.flush()
    """
    Once there is no more reads in the file, but there is still a few unique reads in
    memory, print those last few unique reads.
    """

############################################################

####################
# COMMON FUNCTIONS #
####################

def prepareList(X):
    X=sorted(X)
    z=X[0]
    serial=True
    for i in range(1,len(X)):
        z+=1
        if(X[i]!=z):
            X=X[i-1:]
            serial=False
            break
        else:
            pass
    if(serial==True):X=[max(X)]
    return X

def openfile():
    # open file
    if(args.format=="fasta")or(args.format=="fastq"):
        if(args.gzip):
            handle=gzip.open(args.input,"rb")
        else:
            handle=open(args.input,"rU")
    else:
        sys.stderr.write(">ERROR: --format argument must be \n'fasta' or 'fastq'")
        sys.stderr.flush()
        exit()
    return handle

def fit(x,y): # check if reads match
    r=str(Seq(x,generic_dna).reverse_complement())
    if(x in y)or(y in x):
        return True
    elif(r in y)or(y in r):
        return True
    else:
        return False

def addread(x,y,readdict): # add x to the unique reads
    key=str(x.seq)
    if(y==''):
         # add new entry
        """
        Add the new record to the dictionary as a new entry.
        """
        readdict[key]=[[x],[]]
        if(args.format=="fastq"):
            qual=x.letter_annotations["phred_quality"]
            qual=(sum(qual)/len(qual))
            readdict[key][1].append(qual)
    elif(len(key)>len(y)): # add longer read
        """
        Add the new record in the front of the older records.
        """
        olderrec=readdict[y]
        X=[x]+olderrec[0]
        Y=olderrec[1]
        if(args.format=="fastq"):
            qual=x.letter_annotations["phred_quality"]
            qual=(sum(qual)/len(qual))
            Y=[qual]+Y
        readdict[key]=[X,Y]
        """
        Remover older entry with smaller sequence.
        """
        del readdict[y]
    else:
        """
        Add the new record that is with equal or smaller length than the last record.
        """
        olderrec=readdict[y]
        X=[x]+olderrec[0]
        Y=olderrec[1]
        if(args.format=="fastq"):
            qual=x.letter_annotations["phred_quality"]
            qual=(sum(qual)/len(qual))
            Y=[qual]+Y
        readdict[y]=[X,Y]
        """
        Sorting by length and quality.
        """
        dictitem=readdict[y]
        recordsdict={}
        descriptions=[]
        for i in dictitem[0]:
            recordsdict[i.description]=i
            descriptions+=[i.description]
        sizes=[]
        for i in recordsdict:
            sizes+=[len(recordsdict[i].seq)]
        if(args.format=="fastq"):
            qualities=dictitem[1]
            sizes,qualities,descriptions=zip(*sorted(zip(sizes,qualities,descriptions),reverse=True))
            descriptions=list(descriptions)
            records=[]
            for i in descriptions:
                records+=[recordsdict[i]]
            qualities=list(qualities)
            sizes=[]
            readdict[y]=[records,qualities]
        else:
            sizes,descriptions=zip(*sorted(zip(sizes,descriptions),reverse=True))
            descriptions=list(descriptions)
            records=[]
            for i in descriptions:
                records+=[recordsdict[i]]
            qualities=list(qualities)
            sizes=[]
            readdict[y]=[records,[]]
    """
    The number of duplicates to be kept is 5 or another number provided by the user
    with the arguments -k or --keep.
    """
    if(len(str(x.seq))>len(y)):
        key=str(x.seq)
    else:key=y
    D=readdict[key]
    X=D[0]
    Y=D[1]
    if(len(X)>args.keep):
         # regulate how many duplications are keept
        X=X[:args.keep]
        if(args.format=="fastq"):
            Y=Y[:args.keep]
    readdict[key]=[X,Y]
    return readdict

############################################################

###########################
# PREPARE PARALLELIZATION #
###########################

def createJobs(uniq,skip):
    """
    Once uniq has as many unique reads as processes to run, each of the unique reads
    in uniq will be compared the remaing reads in the input files.
    """
    jobs=[] # current job list
    skipQueue=Queue()
    reportQueue=Queue()
    uniqReads=[]
    for key in uniq:
         # create and start jobs
        record=uniq[key][0][0] # take the first record from the pile
        p=Process(target=minions,args=(record,skip,skipQueue,reportQueue,))
        jobs.append(p)
        p.start()
    for p in jobs:
        skip+=skipQueue.get()
        uniqReads+=reportQueue.get()
        p.join() # wait processes to finish
    for read in uniqReads:
        sys.stdout.write("%s"%(read.format(args.format.lower())))
        sys.stdout.flush()
    skip=list(set(skip)) # reads on this list will be skipped
    skip=prepareList(skip)
    return skip

############################################################

#######################
# MULTIPROC FUNCTIONS #
#######################

def minions(record,skip,skipQueue,reportQueue):
    """
    This function is parallelizable. It will evaluate the uniquieness of a single read.
    """
    uniq={}
    uniq=addread(record,'',uniq)
    readno=0
    handle=openfile()
    queued=[]
    for seqrec in SeqIO.parse(handle,args.format):
        readno+=1
        if(readno<=skip[0])or(readno in skip):
            pass
        else:
            check=False
            for key in uniq:
                check=fit(str(seqrec.seq),key)
                if check==True:
                    uniq=addread(seqrec,key,uniq)
                    queued+=[readno]
                    break
                else:
                    pass
    handle.close()
    reportreads=[]
    for key in uniq:
        reads=uniq[key][0]
        for read in reads:
            reportreads+=[read]
    reportQueue.put(reportreads)
    skipQueue.put(queued)
    return

############################################################

main() # execute functions
exit()