redux.py v20150813 - Copyright (C) 2015 - Denis Jacob Machado

#########
# INDEX #
#########

1. LICENSE
2. AVAILABILITY
3. VERSION
5. DESCRIPTION
6. CRITERIA
7. INPUT
8. OUTPUT
9. ARGUMENTS
10. DEPENDENCIES
11. QUICK TUTORIAL
12. BENCHMARKING

###########
# LICENSE #
###########

redux - Copyright (C) 2015 - Denis Jacob Machado

The redux program comes with ABSOLUTELY NO WARRANTY!

The redux progra is available at www.ib.usp.br/grant/anfibios/researchSoftware.html.

The program may be freely used, modified, and shared under the GNU General Public License
version 3.0 (GPL-3.0, http://opensource.org/licenses/GPL-3.0).
See LICENSE.txt for more details.

################
# AVAILABILITY #
################

* Official URL: http://www.ib.usp.br/grant/anfibios/researchSoftware.html
* GitLab: https://gitlab.com/MachadoDJ/redux
* Online manual: https://gitlab.com/MachadoDJ/redux/wikis/home
* Author's e-mail: denisjacobmachado[at]gmail.com

###########
# VERSION #
###########

version 20150813

###############
# DESCRIPTION #
###############

The redux program is a parallel application tool for duplicate removal.

#############
# CRITERIAS #
#############

The following criteria is used to determine that sequence 1 is a duplicate of sequence 2:
* One of the sequences or its reverse complement must be exactly included into the other

Orderding criteria:
1. Sequence length
2. Average sequence quality

#########
# INPUT #
#########

A single-end sequence file in FASTA or FASTQ format. Files may be compressed with GZIP.

##########
# OUTPUT #
##########

A given number of sequences for each duplication is sent to the standart output (STDOUT).

If the verbose option is on, output verbosity will increase considerably (most likely with
addition time costs). Additional text information will be sent to the standart error
(STDERR).

#############
# ARGUMENTS #
#############

-i, --input: a single input file
-f, --format: format of query sequences ['fasta','fastq'] (default = fastq)
-k, --keep: number of copies to be kept (default = 5)
-p, --processes: maximum processes to run simultaneously (default = max. number of cpus)
-z, --gzip: use when input files are gunziped
-v, --verbose: increase output verbosity
-V, --version: print version number and exit

################
# DEPENDENCIES #
################

Requires Python 2.7.X or newer versions.

The following modules are required:
SeqIO from Bio
* generic_dna from Bio.Alphabet
* Seq from Bio.Seq
* Process,Queue,cpu_count from multiprocessing
* argparse, os and sys


##################
# QUICK TUTORIAL #
##################

This tutorial should work in most UNIX based systems (e.g. Mac OS X, Linux Ubuntu).

Necessary files:
* redux.py
* sample.fastq

Steps:
1. Copy redux.py and sample.fastq to the working directory. Open sample.fastq. See that
the file contains 10 sequence reads.

2. Use redux.py and report up to two duplicates of each sequence. To do so, execute the
following command line:

$ python redux.py -i small.fastq -k 2 -v > selected1.fastq 2> messages.txt

3. Log messages will be printed to messages.txt (standart error is directed with "2>").
Four selected sequences will be printed into "selected1.fastq" (standart output is directed
with ">").

Tips:
To analyse large sequence files in a UNIX system, you may find interesting to used "nohup"
and "&", which will make the analysis run in the background of your machine and will let
you free to close the terminal or use it for other things. For example:

$ cat python redux.py -i large.fastq -k 2 -v > selected2.fastq 2> messages.txt &

In this case, you can use "tail" to monitor the sequences printed into selected.fastq or
the progress messages printed into messages.txt. For example:

$ tail -f messages.txt
[press control + c to quit]
$ tail -f selected2.fastq
[press control + c to quit]

################
# BENCHMARKING #
################

Computer details: MacBook Pro, OS X Version 10.8.5, processor 2.9 GHz Intel Core i7,
memory 8GB 1600 MHz DDR3, Macintosh HD. Python version 2.7.2, Biopython version 1.6.4,
redux version 20150806.

Input: several files with 10 do 1000 sequence reads. Sequences have 101 bp each and are
written in FASTQ format. Each sequence file was analysed 10 times to report the average
time (seconds) and memory usage (maximum resident set size in kbyte).

====================================================================================
Reads | Wall clock | CPU time (user-mode) | CPU time (kernel) | Maximum resident set
------------------------------------------------------------------------------------
10    | 0.196      | 0.158                | 0.064             | 13967360
100   | 0.652      | 0.922                | 0.409             | 13967360
1000  | 22.384     | 54.91                | 4.271             | 13967360
2500  | 126.797    | 332.622              | 12.168            | 13968179.2
5000  | 470.802    | 1284.721             | 28.016            | 13966950.4
------------------------------------------------------------------------------------